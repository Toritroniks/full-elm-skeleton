module Page.Page1 exposing (Model, getSession, getTitle, init, view)

import Html exposing (..)
import Html.Attributes exposing (href)
import Route exposing (Route)
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session
    , msg : String
    }


init : Session -> Model
init session =
    { session = session
    , msg = "Page1 Message"
    }


getSession : Model -> Session
getSession model =
    model.session



-- UPDATE
-- VIEW


view : Model -> Html msg
view model =
    div []
        [ h1
            []
            [ text "Page 1" ]
        , h3
            []
            [ text ("Message: " ++ model.msg) ]
        , a
            [ href (Route.getUrl Route.Page2) ]
            [ text "page2" ]
        ]


getTitle : String
getTitle =
    "Page1"
