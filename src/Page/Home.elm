module Page.Home exposing (Model, Msg, getSession, getTitle, init, update, view)

import Html exposing (..)
import Html.Attributes exposing (href)
import Html.Events exposing (onClick)
import Route exposing (Route)
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session
    , msg : String
    }


init : Session -> Model
init session =
    { session = session
    , msg = "Initial Message"
    }


getSession : Model -> Session
getSession model =
    model.session



-- UPDATE


type Msg
    = ButtonClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ButtonClicked ->
            ( { model | msg = "button clicked!!" }, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1
            []
            [ text "Home Page" ]
        , h3
            []
            [ text ("Message: " ++ model.msg) ]
        , button
            [ onClick ButtonClicked ]
            [ text "Button" ]
        , a
            [ href (Route.getUrl Route.Page1) ]
            [ text "page1" ]
        ]


getTitle : String
getTitle =
    "Home"
