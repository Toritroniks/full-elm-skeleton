import '../index.html';
import { Elm } from './Main';


document.addEventListener('DOMContentLoaded', function() {
  let app = Elm.Main.init({
    node: document.getElementById('elm')
  });
});
