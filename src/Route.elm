module Route exposing (Route(..), fromUrl, getUrl)

import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, oneOf, s, string)


type Route
    = Home
    | Page1
    | Page2


parser : Parser (Route -> a) a
parser =
    oneOf
        [ Parser.map Home Parser.top
        , Parser.map Page1 (s "page1")
        , Parser.map Page2 (s "page2")
        ]


fromUrl : Url -> Maybe Route
fromUrl url =
    Parser.parse parser url


getUrl : Route -> String
getUrl route =
    case route of
        Home ->
            "/"

        Page1 ->
            "/page1"

        Page2 ->
            "/page2"
