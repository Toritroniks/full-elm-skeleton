module Session exposing (Session, getNavKey, newSession)

import Browser.Navigation as Nav


type Session
    = Session Nav.Key


newSession : Nav.Key -> Session
newSession key =
    Session key


getNavKey : Session -> Nav.Key
getNavKey (Session key) =
    key
