module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Page.Home as Home
import Page.Page1 as Page1
import Route exposing (Route)
import Session exposing (Session)
import Url



-- MODEL


type Model
    = NotFound Session
    | Home Home.Model
    | Page1 Page1.Model


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    let
        route =
            Route.fromUrl url

        -- Initial model
        model =
            Home (Home.init (Session.newSession key))
    in
    changeRoute route model



-- UPDATE


type Msg
    = UrlRequested Browser.UrlRequest
    | UrlChanged Url.Url
    | GotHomeMsg Home.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlRequested urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl (Session.getNavKey (getSession model)) (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            changeRoute (Route.fromUrl url) model

        GotHomeMsg homeMsg ->
            case model of
                Home homeModel ->
                    updateWith Home GotHomeMsg model (Home.update homeMsg homeModel)

                _ ->
                    ( model, Cmd.none )


updateWith : (subModel -> Model) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )


getSession : Model -> Session
getSession model =
    case model of
        NotFound session ->
            session

        Home homeModel ->
            Home.getSession homeModel

        Page1 page1Model ->
            Page1.getSession page1Model


changeRoute : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRoute maybeRoute model =
    let
        session =
            getSession model
    in
    case maybeRoute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just Route.Home ->
            ( Home (Home.init session), Cmd.none )

        Just Route.Page1 ->
            ( Page1 (Page1.init session), Cmd.none )

        Just Route.Page2 ->
            Debug.todo "include page 2"



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = getTitle model
    , body =
        [ div
            []
            [ h1
                []
                [ text "Frame" ]
            , div
                []
                [ getRouteView model ]
            ]
        ]
    }


getRouteView : Model -> Html Msg
getRouteView model =
    case model of
        NotFound session ->
            text "404"

        Home homeModel ->
            Html.map GotHomeMsg (Home.view homeModel)

        Page1 page1Model ->
            Page1.view page1Model


getTitle : Model -> String
getTitle model =
    case model of
        NotFound session ->
            "404"

        Home homeModel ->
            Home.getTitle

        Page1 page1Model ->
            Page1.getTitle



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }
