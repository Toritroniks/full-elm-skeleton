const webpack = require('webpack');
const path = require('path');

const MODE = process.env.npm_lifecycle_event === 'build' ? 'production' : 'development';

module.exports = {
  mode: MODE,
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        exclude: [/elm-stuff/, /node_modules/],
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
        },
      },
      {
        test: [/\.elm$/],
        exclude: [/elm-stuff/, /node_modules/],
        use: [
          { loader: 'elm-hot-webpack-loader' },
          {
            loader: 'elm-webpack-loader',
            options: MODE === 'production' ? {} : { debug: true, forceWatch: true },
          },
        ],
      },
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        exclude: [/elm-stuff/, /node_modules/],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js', '.elm'],
  },
  plugins: [
    // Suggested for hot-loading
    new webpack.NamedModulesPlugin(),
    // Prevents compilation errors causing the hot loader to lose state
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  devServer: {
    inline: true,
    stats: 'errors-only',
    historyApiFallback: {
      index: 'index.html'
    },
  },
};
